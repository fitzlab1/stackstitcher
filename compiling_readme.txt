Get IntelliJ Idea (Java IDE) -- community edition is free.

Open the StackStitcher subdirectory as a project.

You can then Build -> Make Project to compile, or Run it.

Build -> Build Artifacts will produce a .jar file of the StackStitcher source.

To distribute the result, you can put the resulting .jar in a directory containing a copy of the lib folder, zip it, and send it out. 

A cleaner way is to produce a fat jar that includes the libraries in the lib directory. JarSplice is a good tool for producing fat jars:
http://ninjacave.com/jarsplice

To make the fat jar, run jarsplice and:
- Add all jars (StackStitcher.jar + lib jars)
- No natives
- set the main class to: confocaltools.ConfocalToolsApp
- then create.

